Configuration
=============

ADMan configuration is done using a `YAML`_ file.

.. _config_default_path:

Default path
------------
By default, Adman looks for its config file at:

- ``/etc/adman/config.yml`` when run as ``root``
- ``~/.config/adman/config.yml`` when run as a normal user

Types
-------------------
ADMan configuration items expect inputs of certain types. The following standard YAML types are used:

- *boolean* -- ``True`` or ``False``
- *int* -- A decimal integer
- *string* -- A string of text; recommended to be elcosed in quotes
- *list<T>* -- A YAML list of values of another type


Additionally, the following custom "types" are defined:

.. _config_type_path:

*path* -- A YAML string; the path to a file. The path can either be absolute
(start with a leading ``/``), or relative to the directory containing the
``config.yml`` file.

.. _config_type_range:

*range* -- A YAML mapping with required ``min`` and ``max`` integers

.. _config_type_duration:

*duration* -- A string describing duration of time, expressed as ``N UNITS`` where ``N`` is an integer and ``UNITS`` is a unit of time e.g. "days"

.. _config_type_containers:

*containers* -- A YAML mapping where:

- The keys identify an LDAP container by its DN, relative to the domain DN. Put
  another way, they are one or more `RDN`_ strings joined by commas. See the
  example below.
- The values are a mapping with the following keys:

  .. list-table::
     :widths: 20 20 20 40
     :header-rows: 1

     * - Config Key
       - Type
       - Default
       - Description
     * - ``scope``
       - *string*
       - "subtree"
       - Scope of LDAP search in the container:

         - ``one`` (this container only)
         - ``subtree`` (this container and all child containers)

- If there are no keys (i.e., YAML ``null`` or ``{}``), then no containers are
  considered.
- Some uses of the *containers* type may accept the string ``all`` to mean "all
  containers in the domain", and may default to this if the entire config
  setting is omitted.

.. _RDN: https://ldapwiki.com/wiki/Relative%20Distinguished%20Name
.. todo: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-adts/b645c125-a7da-4097-84a1-2fa7cea07714#gt_22198321-b40b-4c24-b8a2-29e44d9d92b9

Common settings
-------------------
These settings apply to ADMan as a whole, or multiple :doc:`tasks/index`.
Feature-specific configuration are described on each feature's page.

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``domain``
     - *string*
     - *(required)*
     - DNS name of domain (e.g. ``ad.example.com``)
   * - ``changelog``
     - :ref:`path<config_type_path>`
     - *(standard error)*
     - Path to file where changes are written
   * - ``ldap_auth``
     - :ref:`config_ldap_auth`
     - *(required)*
     - LDAP authentication options
   * - ``smtp``
     - :ref:`config_smtp`
     - (required [#reqsmtp]_)
     - Settings for sending email


.. note::
   .. [#reqsmtp] ``smtp`` is required if any of the following are used:

      - :doc:`tasks/passwd-exp-notify`
      - :doc:`tasks/find-stale` ``stale_accounts.email_to`` config




.. _config_ldap_auth:

``ldap_auth`` settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``mode``
     - *string*
     - *(required)*
     - LDAP authentication mode -- choices:

       - ``gssapi`` -- Use Kerberos via GSSAPI_

   * - ``krb_username``
     - *string*
     - (required [#reqkrb]_)
     - Kerberos username
   * - ``krb_keytab``
     - :ref:`path<config_type_path>`
     - (required [#reqkrb]_)
     - Kerberos keytab path (see :doc:`setup`)
   * - ``krb_cache``
     - :ref:`path<config_type_path>`
     - (required [#reqkrb]_)
     - Kerberos credential cache path


.. note::

   .. [#reqkrb] ``ldap_auth.krb_*`` options are required if ADMan is to automatically manage kerberos tickets. These can be left unset if adman is to use the current user's ticket.



.. _config_smtp:

``smtp`` settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``email_from``
     - *string*
     - *(required)*
     - Email address from which messages should be sent
   * - ``host``
     - *string*
     - "localhost"
     - SMTP server hostname/IP to which messages should be sent
   * - ``port``
     - *integer*
     - 25 (or 465 for SSL)
     - SMTP server port number
   * - ``username``
     - *string*
     - *(none)*
     - SMTP username
   * - ``password``
     - *string*
     - | *(none)*
       | *(req'd w/ username)*
     - SMTP password
   * - ``encryption``
     - *string*
     - *(none)*
     - SMTP server encryption node; one of:

       - (blank) -- Not encrypted
       - ``starttls`` -- Opportunistic TLS (via ``STARTTLS``)
       - ``ssl`` -- Implicit (mandatory) SSL/TLS



Example config file
-------------------

.. literalinclude:: ../example_config.yml
   :language: yaml


.. _YAML: https://yaml.org/
.. _GSSAPI: https://en.wikipedia.org/wiki/Generic_Security_Services_Application_Program_Interface
