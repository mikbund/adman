User directory creation
=======================

ADMan can create per-user directories in any number of base directories on
remote SMB servers. It will configure permissions and can even create a default
set of subdirectories.

Actions
-------------------------------------------------
For each configured *userdirs* entry, ADMan will:

- Create a directory under ``basepath`` for each user, as limited by the ``only`` specification
- Set the owner a according to ``owner`` and ``group``
- Add templated access control entries to the ACL according to ``acl``
- Create a set of subdirectories under the user directory, and set the owner, group, and ACL


Configuration
-------------------------------------------------
``userdirs`` is a *list* of entries, each (starting with a hyphen) with the
following sub-keys:
 
.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``basepath``
     - *string*
     - *(required)*
     - UNC path of the directory in which to create each userdir
   * - ``only``
     - :ref:`containers<config_type_containers>`
     - ``'all'``
     - LDAP containers for which userdir creation will be limited
   * - ``owner``
     - *string*
     - *None*
     - | The user account name to set as the owner of each userdir.
       | If None, left as-is after creation.
       | Can include template variables: ``${user}``
   * - ``group``
     - *string*
     - *None*
     - | The group account name to set as the group of each userdir
       | If None, left as-is after creation.
       | Can include template variables: ``${user}``
   * - ``acl``
     - list<:ref:`tmpl_ace<config_type_tmpl_ace>`>
     - | A list of access control entries.
     - *(empty)*
       | Can include template variables: ``${user}``
   * - ``subdirs``
     - list<:ref:`subdir<config_type_subdir>`>
     - *(empty)*
     - A list of additional subdirectories to create in each user's directory


.. _config_type_subdir:

*subdirs* -- A *mapping* with the following sub-keys:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``name``
     - *string*
     - *(required)*
     - Name of the directory to create (in each user directory)
   * - ``acl``
     - list<:ref:`tmpl_ace<config_type_tmpl_ace>`>
     - *(empty)*
     - | A list of access control entries.
       | Can include template variables: ``${user}``

.. _config_type_tmpl_ace:

*tmpl_ace* -- A *string*, representing an Access Control List Entry (ACE),
with the following format::

    sid_or_name:type/flags/mask

- ``sid_or_name`` -- Either a `SID`_
  (e.g. ``S-1-5-21-1004336348-1177238915-682003330-512``)
  or principal name (e.g. ``Domain Users``), to which the ACE applies.
  Can include template variables: ``${user}``

- ``type`` -- The type of ACE (see `ACE_HEADER`_)

    - ``0`` -- Access Allowed (typical ACE usage)
    - ``1`` -- Access Denied

- ``flags`` -- A decimal integer of ACE flags which can be ORed together (see `ACE_HEADER`_)

    - ``0x01`` -- ``OBJECT_INHERIT``
    - ``0x02`` -- ``CONTAINER_INHERIT``
    - ``0x04`` -- ``NO_PROPAGATE_INHERIT``
    - ``0x08`` -- ``INHERIT_ONLY``

  Example: ``11`` (``OBJECT_INHERIT | CONTAINER_INHERIT | INHERIT_ONLY``)

- ``mask`` -- A hexadecimal integer of access flags which can be ORed together (see `ACCESS_MASK`_)

    - ``0x00000001`` -- File: Read data / Directory: List
    - ``0x00000002`` -- File: Write data / Directory: Add file
    - ``0x00000004`` -- File: Append data / Directory: Add subdirectory
    - ``0x00000008`` -- File/Directory: Read extended attributes
    - ``0x00000010`` -- File/Directory: Write extended attributes
    - ``0x00000020`` -- File: Execute / Directory: Traverse
    - ``0x00000040`` -- Directory: Delete child
    - ``0x00000080`` -- File/Directory: Read attributes
    - ``0x00000100`` -- File/Directory: Write attributes

    - ``0x00010000`` -- Delete an object
    - ``0x00020000`` -- Read the security descriptor of an object
    - ``0x00040000`` -- Change the access control list of an object
    - ``0x00080000`` -- Change the owner of an object
    - ``0x00100000`` -- Synchronize or wait on the object

  Common combinations:

    - ``0x00120089`` -- ``SEC_RIGHTS_FILE_READ`` / ``SEC_RIGHTS_DIR_READ``
    - ``0x001200a0`` -- ``SEC_RIGHTS_FILE_EXECUTE`` / ``SEC_RIGHTS_DIR_EXECUTE``
    - ``0x00120116`` -- ``SEC_RIGHTS_FILE_WRITE`` / ``SEC_RIGHTS_DIR_WRITE``
    - ``0x001f01ff`` -- ``SEC_RIGHTS_FILE_ALL`` / ``SEC_RIGHTS_DIR_ALL``

.. note:: In the future, we hope to support `SDDL`_ for expressing ACLs.
   See `issue 18`_.



Example configuration
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_config.yml
   :language: yaml
   :start-at: userdirs
   :end-before: end userdirs


.. _SID: https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/security-identifiers
.. _ACE_HEADER: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dtyp/628ebb1d-c509-4ea0-a10f-77ef97ca4586
.. _ACCESS_MASK: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dtyp/7a53f60e-e730-4dfe-bbe9-b21b62eb790b
.. _SDDL: https://docs.microsoft.com/en-us/windows/win32/secauthz/security-descriptor-definition-language
.. _issue 18: https://gitlab.com/JonathonReinhart/adman/-/issues/18

Commands
-------------------------------------------------
Relevant CLI commands:

- :ref:`command_user_mkdirs`
