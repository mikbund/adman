Command-Line Interface
======================


Synopsis
--------
.. code-block::

   adman [-h] [-c CONFIG] [-v] [--version]
         [--loglevel LEVEL]
         COMMAND ...

Global Options:
  -h, --help            Show help message and exit
  --version             Show ADMan version and exit
  --loglevel LEVEL      Set the logging level (default: WARNING)

                        Options: DEBUG,INFO,WARNING,ERROR,CRITICAL

  -c CONFIG, --config CONFIG
                        Alternate path to config file
  -v, --verbose         Show verbose output


Commands
--------

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Command
     - Description

   * -
     - **Top-level commands**
   * - :ref:`command_allmaint`
     - Perform all automated maintenance (assign IDs, UPNs)
   * - :ref:`command_assignids`
     - Assign all missing ``*idNumber`` attributes
   * - :ref:`command_clearids`
     - Clear all ``*idNumber`` attributes
   * - :ref:`command_exec`
     - Execute a command in Kerberos context
   * - :ref:`command_findstale`
     - Find stale accounts and report/disable per config

   * - **computer**
     - **Computer sub-commands**
   * - :ref:`command_computer_assign`
     - Assign missing ``uidNumber`` attributes
   * - :ref:`command_computer_list`
     - List computers

   * - **group**
     - **Group sub-commands**
   * - :ref:`command_group_assign`
     - Assign missing ``gidNumber`` attributes
   * - :ref:`command_group_list`
     - List groups

   * - **state**
     - **State sub-commands**
   * - :ref:`command_state_list`
     - List state information
   * - :ref:`command_state_init`
     - Initialize state information

   * - **user**
     - **User sub-commands**
   * - :ref:`command_user_assign`
     - Assign missing ``uidNumber`` attributes
   * - :ref:`command_user_checkexpire`
     - Check for expiring/expired passwords
   * - :ref:`command_user_setupns`
     - Set ``userPrincipalName`` attributes
   * - :ref:`command_user_list`
     - List users
   * - :ref:`command_user_mkdirs`
     - Make user directories

Top-level commands
________________________________________________________________________________

.. _command_allmaint:

``allmaint``
~~~~~~~~~~~~
Shortcut command which runs all* automated maintenance commands:

- :ref:`command_assignids`
- :ref:`command_user_setupns`
- :ref:`command_user_checkexpire`
- :ref:`command_user_mkdirs`

.. note::
   The :ref:`command_allmaint` command does *not* include
   :ref:`command_findstale`, as that will usually be done on a much longer
   interval.



.. _command_assignids:

``assignids``
~~~~~~~~~~~~~
Shortcut command which runs the following :doc:`tasks/id-assign` commands:

- :ref:`command_group_assign`
- :ref:`command_user_assign`
- :ref:`command_computer_assign` (if configured)



.. _command_clearids:

``clearids``
~~~~~~~~~~~~~
This command will clear all ``*idNumber`` attributes for the configured:

- Group ``gidNumber``
- User ``uidNumber`` & ``gidNumber``
- Computer ``uidNumber`` & ``gidNumber``



.. _command_exec:

``exec``
~~~~~~~~~~~~~
*(Added in v0.6.0)* This command enables running arbitrary command lines in the
ADMan Kerberos context. This is useful for ``samba-tool`` commands which
support Kerberos.

Example:

.. code-block:: console

    $ adman exec samba-tool domain backup online --server=dc1.example.com --targetdir=domainbakup -k yes



.. _command_findstale:

``findstale``
~~~~~~~~~~~~~
*(Added in v0.7.0)*

This command will find stale user/computer accounts and disable them as
configured. If configured, it will send a report to the admin.

See :doc:`tasks/find-stale`.


Computer commands
________________________________________________________________________________
.. _command_computer_assign:

``computer assign``
~~~~~~~~~~~~~~~~~~~~
See :ref:`command_user_assign`.



.. _command_computer_list:

``computer list``
~~~~~~~~~~~~~~~~~~~~
List all computers.



Group commands
________________________________________________________________________________
.. _command_group_assign:

``group assign``
~~~~~~~~~~~~~~~~~~~~
This :doc:`tasks/id-assign` command will:

- Assign ``gidNumber`` values to all configured groups.

  - The next ``gidNumber`` to be assigned is stored in ``msSFU30MaxGidNumber``.



.. _command_group_list:

``group list``
~~~~~~~~~~~~~~~~~~~~
List all groups.



State commands
________________________________________________________________________________
These commands interact with the :ref:`ADMan-related state <id_assign_state>`
recorded in LDAP.

.. _command_state_list:

``state list``
~~~~~~~~~~~~~~~~~~~~
List the current state:

.. code-block:: console

   $ adman state list
   Next uidNumber: 100011
   Next gidNumber: 100008

.. _command_state_init:

``state init``
~~~~~~~~~~~~~~~~~~~~
Initialize the ADMan state.

.. code-block::

   adman state init [-h] [--force | --ignore]

Options:
  -h, --help            Show help message and exit
  --force               Force re-initialization; overwrite existing values with ``MAX(xidNumber)+1``
  --ignore              Ignore partially-initialized state and initialize other values

This command evaluates the ``uidNumber``/``gidNumber`` values currently
assigned to users/groups, and sets the "Next uidNumber" and "Next gidNumber"
values accordingly:

- If no ``xidNumber`` are currently assigned, sets "next" to the beginning of
  the configured range.
- Otherwise, sets "next" to ``MAX(xidNumber)+1``.

If the state is already initialized and is as expected, nothing is done:

.. code-block:: console

   $ adman state init
   Next uidNumber: 100011
   Next gidNumber: 100008

If the state is already initialized but doesn't match the expected value, an
error is printed:

.. code-block:: console

   $ adman state init
   Next uidNumber: 100011
   Next gidNumber: 100008

   Error: Domain state next_uid already set to 100011, doesn't match expected 100008
   Use --force or --ignore


User commands
________________________________________________________________________________

.. _command_user_assign:

``user assign``
~~~~~~~~~~~~~~~~~~~~
This :doc:`tasks/id-assign` command will:

- Assign ``uidNumber`` values to all configured users.

  - The next ``uidNumber`` to be assigned is stored in ``msSFU30MaxUidNumber``.

- Update the ``gidNumber`` to match that of the user's primary group (``primaryGroupID``).



.. _command_user_checkexpire:

``user checkexpire``
~~~~~~~~~~~~~~~~~~~~
*(Added in v0.2.0)*

This command will send an email to users whose passwords are expiring in the
configured time window.

See :doc:`tasks/passwd-exp-notify`.


.. _command_user_setupns:

``user setupns``
~~~~~~~~~~~~~~~~~~~~
This command will update users' ``userPrincipalName`` attribute if necessary to
match the configured UPN suffix.

See :doc:`tasks/upn-suffix`.


.. _command_user_list:

``user list``
~~~~~~~~~~~~~~~~~~~~
List all users.



.. _command_user_mkdirs:

``user mkdirs``
~~~~~~~~~~~~~~~~~~~~
This command will create per-user directories as configured.

See :doc:`tasks/userdir`.
