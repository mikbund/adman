# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.9.0 - 2022-06-25
- Allow explicitly empty "container" config items (!36, !38)
- Add `include_disabled` config to `stale_accounts` (!37)
- Update stale account report header (!39)

## 0.8.0 - 2021-12-12
- Only connect to DC holding PDC Emulator FSMO role for single-master safety (!26)
- Switch from unittest to pytest-based tests (!29)
- Improve LDAP object model, adding tests and using LDAP REPLACE to update
  non-atomic attributes (!28)
- Ensure LDAP attributes used to hold next uid/gid values are updated
  atomically (!30)

## 0.7.3 - 2021-12-05
- Write to changelog when users are disabled (!27)

## 0.7.2 - 2021-11-22
- Add HTML format to stale account report (!25)

## 0.7.1 - 2021-11-06
- Fix missing dnsdomain attribute (!24)

## 0.7.0 - 2021-11-06
- Add `findstale` command and `stale_accounts` config which enables finding and
  disabling stale user or computer accounts, and sending an email to the
  administrator (!23)
- Allow running without `krb_*` config parameters being set (!18)

## 0.6.1 - 2021-10-23
- Fix error parsing klist output due to new "Ticket server" line
  in krb5 1.18 (!20)

## 0.6.0 - 2021-05-21
- Add `exec` command for running arbitrary commands in ADMan kerberos context

## 0.5.1 - 2021-02-02
- Run `klist` using the 'C' locale to avoid locale-induced date parsing
  errors (!16)
- Make pysmbc an optional dependency, only needed fo `user mkdirs`
  command (!17)

## 0.5.0 - 2020-03-17
- Fix unnecessary Kerberos ticket request on every invocation (!11)
- Make `id_assign` config section optional (!12)
- Add `user mkdirs` option to create configured `userdirs` (!13)`
- Exclude disabled users from all assignments (!14)

## 0.4.0 - 2020-02-18
- Add new `only` key to `id_assign` config which enables restricting the
  containers whose users and groups are assigned `uidNumber`/`gidNumber`
  attributes (!8)
- Add timestamps to changelog output (!9)
- Move changelog path from command line option to config file (!10)

## 0.3.0 - 2020-02-16
- Move `uid_range`/`gid_range` configuration options under a new `id_assign` key
- Assign gidNumber to computer groups (e.g. `Domain Computers`) and assign
  uidNumber to computer objects. This new behavior is on by default but can be
  disabled via new `id_assign.computers` key. (!7)

## 0.2.3 - 2020-02-11
- Change kerberos code to be Python 3.5 compatible (#14)
- Don't require smtp config if email won't be used (#9)

## 0.2.2 - 2020-01-17
- Fix issue where a domain with password expiry disabled would lead to a
  datetime error (#11)

## 0.2.1 - 2020-01-06
- Fix issue where an expiring TGT can lead to a GSSAPI error (#10).
- Add -v option for 'user list' and 'group list'

## 0.2.0 - 2019-12-26
- Add pending password expiry notification feature
  - Added 'user checkexpire' command and related configuration
  - Added SMTP configuration
- Set default config and data paths
- Add --version option
- Fix exception when domain has no configured alternate UPN suffixes
- Refactor LdapObject code to simplify handling of known attributes

## 0.1.0 - 2019-07-18
- Add support for UPN suffix assignment via `user setupns` command
- Rename `assign`/`clear` to `assignids`/`clearids`
- Add `allmaint` command

## 0.0.2 - 2019-07-16
- Rename to "adman"

## 0.0.1 - 2019-06-23
- Initial release as "adam"
